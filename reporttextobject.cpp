#include "reporttextobject.h"

ReportTextObject::ReportTextObject(
        QObject *parent,
        QHash<QString, QString> *dictionary,
        QDomNode *elementNodeSource,
        QDomNodeList *elementNodesData,
        QHash<int, QString> *blobstore) : ReportBandContainerObject(parent)
{
    this->dictionary = dictionary;
    this->elementNodeSource = elementNodeSource;
    this->elementNodesData = elementNodesData;
    this->blobstore = blobstore;
}

ReportTextObject::~ReportTextObject()
{

}

void ReportTextObject::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

}

void ReportTextObject::parseNode()
{

}

void ReportTextObject::drawElement(QGraphicsScene *scene)
{

}
