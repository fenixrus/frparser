#ifndef REPORTTEXTOBJECT_H
#define REPORTTEXTOBJECT_H

#include "reportbandcontainerobject.h"

class ReportTextObject : public ReportBandContainerObject
{
public:
    ReportTextObject(QObject *parent,
                     QHash <QString, QString> *dictionary,
                     QDomNode *pageNodeSource,
                     QDomNodeList *elementNodesData,
                     QHash<int, QString> *blobstore);
    ~ReportTextObject();
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void parseNode();
    void drawElement(QGraphicsScene *scene);
};

#endif // REPORTTEXTOBJECT_H
