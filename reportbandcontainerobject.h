#ifndef REPORTBANDCONTAINEROBJECT_H
#define REPORTBANDCONTAINEROBJECT_H

#include "reportbaseobject.h"
#include <QGraphicsItem>

class ReportBandContainerObject : public ReportBaseObject, public QGraphicsItem
{
    Q_OBJECT
public:
    explicit ReportBandContainerObject(QObject *parent);
    virtual ~ReportBandContainerObject();
    virtual void drawElement(QGraphicsScene *scene);
signals:

public slots:

protected:
    QDomNode *elementNodeSource;
    QDomNodeList *elementNodesData;

private:

};

#endif // REPORTBANDCONTAINEROBJECT_H
