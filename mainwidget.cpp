#include "mainwidget.h"
#include "ui_mainwidget.h"

MainWidget::MainWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainWidget)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()),
            this, SIGNAL(signalOpenTemplate()));
}

MainWidget::~MainWidget()
{
    delete ui;
}

void MainWidget::setFileNameText(QString fileName)
{
    ui->label_3->setText(fileName);
}

void MainWidget::setModelTreeView(QStandardItemModel *model)
{
    ui->treeView->setModel(model);
}

bool MainWidget::getShowBorders()
{
    return ui->checkBox->isChecked();
}

bool MainWidget::getUsingStandartTemplate()
{
    return ui->checkBox_2->isChecked();
}
