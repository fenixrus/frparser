#include "controlviewwidget.h"//Подключение заголовочного файла

ControlViewWidget::ControlViewWidget(QObject *parent) : QObject(parent)
{
    mainWindow = new MainWidget;
    //Инициализируем и выделяем память под объект главного меню
    connect(this->mainWindow, SIGNAL(signalOpenTemplate()),
            this, SLOT(slotOpenTemplate()));
    QRect screenRect = QApplication::screens().at(0)->geometry();
    int width = mainWindow->width();
    int height =mainWindow->height();
    int x = (screenRect.width() - width)/2;
    int y = (screenRect.height() - height)/2;
    mainWindow->setGeometry(QRect(x, y, width, height));//
    mainWindow->show();
    documentViewWindow = new QGraphicsView;
    scene  = new QGraphicsScene;
    model = new QStandardItemModel;
    _ "ControlViewWidget";
}

ControlViewWidget::~ControlViewWidget()
{
    _ "~ControlViewWidget";
    //Вызывается в самую последнюю очередь жизнидеятельности программы
}

void ControlViewWidget::slotOpenTemplate()
{
    float pixelPerMm = QApplication::screens().at(0)->logicalDotsPerInch()/2.54/10;
    float widthA4 = pixelPerMm*210;//Ширина
    QRect screenRect = QApplication::screens().at(0)->geometry();
    QRect viewRect((screenRect.width()-widthA4)/2-20, 40, widthA4+20, screenRect.height()-100);
    QString fileName("Simple_List_1.fpx");

    if (!mainWindow->getUsingStandartTemplate())//Получение пути для открытия шаблона
    {
        fileName = QFileDialog::getOpenFileName(
                    mainWindow,
                    "Выберете шаблон",
                    "D:/qt_projects",
                    "*.fpx");
    }
    model->clear();
    scene->clear();

    if (fileName.isEmpty()) return;

    FormatGraphScene formatingScene(this, scene, pixelPerMm, model);
    connect(&formatingScene, SIGNAL(signalFinishedFormat()),
            this, SLOT(slotFinishedFormat()));
    mainWindow->setFileNameText(fileName);
    formatingScene.formation(fileName);
    documentViewWindow->setGeometry(viewRect);
    documentViewWindow->setWindowModality(Qt::ApplicationModal);
    documentViewWindow->setAlignment(Qt::AlignHCenter);
    //documentViewWindow->show();
}

void ControlViewWidget::slotFinishedFormat()
{
    documentViewWindow->setScene(scene);
    mainWindow->setModelTreeView(model);
}
