#include "formatgraphscene.h"

FormatGraphScene::FormatGraphScene(QObject *parent,
                                   QGraphicsScene *scene,
                                   float ArgPixelPerMm,
                                   QStandardItemModel *ArgModel) :
    QObject(parent),
    graphicsScene(scene),
    pixelPerMm(ArgPixelPerMm),
    model(ArgModel)
{
    _ "FormatGraphScene";
}

FormatGraphScene::~FormatGraphScene()
{
    _ "";
    _ "~FormatGraphScene";
}

void FormatGraphScene::formation(QString reportFileName)
{
    _ "---------------------------------------";
    _  reportFileName;
    _  "---------------------------------------";
    _ "";

    QFile *reportFile = new QFile(reportFileName);
    if(!reportFile->open(QIODevice::ReadOnly))
    {
        delete reportFile;
        return;
    }

    QByteArray *decompressedReportFile = new QByteArray;

    decompressedReportFile->append(GZipArchive::decompress(reportFile->readAll()));
    reportFile->close();
    delete reportFile;

    QDomDocument domDoc;

    if(!domDoc.setContent(*decompressedReportFile)) return;

    decompressedReportFile->clear();
    delete decompressedReportFile;

    QHash <QString,QString> dictionary;
    QHash <int,QString> blobstore;
    QDomNode dictionaryNode = domDoc.elementsByTagName("dictionary").at(0);
    QDomNode sourcepagesNode = domDoc.elementsByTagName("sourcepages").at(0);
    QDomNode blobstoreNode = domDoc.elementsByTagName("blobstore").at(0);

    QDomNode n;

    n = dictionaryNode.firstChild();

    while(!n.isNull())
    {
        QString name = "Page";
        QStringList namePart = n.toElement().attribute("name").split('.');
        QString pageNumber = namePart.at(0);
        name += QString::number(pageNumber.toInt() + 1);
        if (namePart.count() > 1)
            name += "." + namePart.at(1);
        dictionary.insert(name, n.nodeName());
        n = n.nextSibling();
    }

    int blobstoreIndex=0;
    n = blobstoreNode.firstChild();
    while(!n.isNull())
    {
        blobstore.insert(++blobstoreIndex, n.toElement().attribute("Stream"));
        n = n.nextSibling();
    }


    QDomNode nextPageSource = sourcepagesNode.firstChild();

    while(!nextPageSource.isNull())
    {
        QDomNodeList pageNodesData = domDoc.elementsByTagName(nextPageSource.toElement().attribute("Name").toLower());
        ReportPageObject reportPage(this, &dictionary, &nextPageSource, &pageNodesData, &blobstore);
        reportPage.parseNode();
        nextPageSource = nextPageSource.nextSibling();
    }

    QStandardItem *parentItemSourcepages = new QStandardItem("PreparedReport");
    QDomNode preparedreportNode = domDoc.elementsByTagName("preparedreport").at(0);

    model->appendRow(parentItemSourcepages);
    recursiveParsing(preparedreportNode, parentItemSourcepages);
    bandNodes.insert("preparedreport", parentItemSourcepages);

    emit signalFinishedFormat();
}

void FormatGraphScene::recursiveParsing(QDomNode parentXmlModel, QStandardItem *parentItemModel)
{
    QDomNode n = parentXmlModel.firstChild();

    while(!n.isNull())
    {
        //_ n.nodeName() <<  "  =  " << n.toElement().attribute("Name") << "parent = " << parentXmlModel.nodeName();
        QString text = n.nodeName();
        if (!n.toElement().attribute("Name").isEmpty())
            text.append(" = " + n.toElement().attribute("Name"));
        QStandardItem *itemModel = new QStandardItem(text);

        QDomNamedNodeMap atrs =  n.attributes();
        for (int i = 0; i < atrs.count(); ++i)
        {
            QDomNode atr = atrs.item(i);
            QStandardItem *itemModelAtr = new QStandardItem(atr.nodeName() + " : " + atr.nodeValue());

            QFont currFont = itemModelAtr->font();
            currFont.setItalic(true);
            itemModelAtr->setFont(currFont);

            itemModel->appendRow(itemModelAtr);
        }

        if (!n.firstChild().isNull())//Бэнд
        {
            QFont currFont = itemModel->font();
            currFont.setBold(true);
            itemModel->setFont(currFont);
            parentItemModel->appendRow(itemModel);
            bandNodes.insert(n.toElement().attribute("Name"), itemModel);
            recursiveParsing(n, itemModel);
        }
        else
        {
            bandNodes.value(n.parentNode().toElement().attribute("Name"))->appendRow(itemModel);
        }

        n = n.nextSibling();
    }
}
