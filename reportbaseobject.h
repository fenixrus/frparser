#ifndef REPORTBASEOBJECT_H
#define REPORTBASEOBJECT_H

#include <QObject>
#include <QtXml>
#include <QDebug>
#define _ qDebug() <<

class ReportBaseObject : public QObject
{
    Q_OBJECT
public:
    explicit ReportBaseObject(QObject *parent);
    virtual ~ReportBaseObject();
    virtual void parseNode() = 0;

signals:

public slots:

protected:
    QHash <QString, QString> *dictionary;
    QHash<int, QString> *blobstore;

private:

};

#endif // REPORTBASEOBJECT_H
