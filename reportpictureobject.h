#ifndef REPORTPICTUREOBJECT_H
#define REPORTPICTUREOBJECT_H

#include "reportbandcontainerobject.h"

class ReportPictureObject : public ReportBandContainerObject
{
    Q_OBJECT
public:
    ReportPictureObject(QObject *parent,
                        QHash <QString, QString> *dictionary,
                        QDomNode *elementNodeSource,
                        QDomNodeList *elementNodesData,
                        QHash<int, QString> *blobstore);
    ~ReportPictureObject();
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void parseNode();
    void drawElement(QGraphicsScene *scene);
};

#endif // REPORTPICTUREOBJECT_H
