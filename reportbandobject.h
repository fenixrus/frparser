#ifndef REPORTBANDOBJECT_H
#define REPORTBANDOBJECT_H

#include "reportbaseobject.h"

class ReportBandObject : public ReportBaseObject
{
    Q_OBJECT
public:
    explicit ReportBandObject(QObject *parent = 0);
    ~ReportBandObject();

signals:

public slots:
};

#endif // REPORTBANDOBJECT_H
