#ifndef REPORTPAGEOBJECT_H
#define REPORTPAGEOBJECT_H

#include "reportbaseobject.h"

class ReportPageObject : public ReportBaseObject
{
    Q_OBJECT
public:
    explicit ReportPageObject(QObject *parent,
            QHash <QString, QString> *dictionary,
            QDomNode *pageNodeSource,
            QDomNodeList *pageNodesData,
            QHash<int, QString> *blobstore);
    ~ReportPageObject();
    void parseNode();
    float getTopOffset();
    float getLeftOffset();
signals:

public slots:

private:
    QDomNode *pageNodeSource;
    QDomNodeList *pageNodesData;
    float topOffset;
    float leftOffset;

};

#endif // REPORTPAGEOBJECT_H
