#ifndef GZIPARCHIVE_H
#define GZIPARCHIVE_H

#include <QObject>
#include <zlib.h>

#define GZIP_WINDOWS_BIT 15 + 16
#define GZIP_CHUNK_SIZE 32 * 1024

class GZipArchive : public QObject
{
    Q_OBJECT
public:
    explicit GZipArchive(QObject *parent = 0);
    static QByteArray compress(QByteArray input);
    static QByteArray decompress(QByteArray input);

signals:

public slots:
};

#endif // GZIPARCHIVE_H
