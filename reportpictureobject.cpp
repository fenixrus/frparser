#include "reportpictureobject.h"

ReportPictureObject::ReportPictureObject(
        QObject *parent,
        QHash<QString, QString> *dictionary,
        QDomNode *elementNodeSource,
        QDomNodeList *elementNodesData,
        QHash<int, QString> *blobstore) :
    ReportBandContainerObject(parent)
{
    this->dictionary = dictionary;
    this->elementNodeSource = elementNodeSource;
    this->elementNodesData = elementNodesData;
    this->blobstore = blobstore;
}

ReportPictureObject::~ReportPictureObject()
{

}

void ReportPictureObject::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

}

void ReportPictureObject::parseNode()
{

}

void ReportPictureObject::drawElement(QGraphicsScene *scene)
{

}
