#-------------------------------------------------
#
# Project created by QtCreator 2016-03-22T11:22:12
#
#-------------------------------------------------

QT       += core widgets xml

TARGET = FRParser
TEMPLATE = app
CONFIG += c++11

SOURCES += \
    controlviewwidget.cpp \
    main.cpp \
    mainwidget.cpp \
    formatgraphscene.cpp \
    gziparchive.cpp \
    reportbaseobject.cpp \
    reportpageobject.cpp \
    reportbandobject.cpp \
    reportbandcontainerobject.cpp \
    reporttextobject.cpp \
    reportpictureobject.cpp

HEADERS  += \
    controlviewwidget.h \
    mainwidget.h \
    formatgraphscene.h \
    gziparchive.h \
    reportbaseobject.h \
    reportpageobject.h \
    reportbandobject.h \
    reportbandcontainerobject.h \
    reporttextobject.h \
    reportpictureobject.h
    
FORMS    += \
    mainwidget.ui

LIBS += \
 -lz

RESOURCES += \
    img.qrc

