#include "reportpageobject.h"

ReportPageObject::ReportPageObject(QObject *parent,
                                   QHash<QString, QString> *dictionary,
                                   QDomNode *pageNodeSource,
                                   QDomNodeList *pageNodesData,
                                   QHash<int, QString> *blobstore) :
    ReportBaseObject(parent)
{
    this->dictionary = dictionary;
    this->pageNodeSource = pageNodeSource;
    this->pageNodesData = pageNodesData;
    this->blobstore = blobstore;
    topOffset = 10;
    leftOffset = 10;
}

ReportPageObject::~ReportPageObject()
{

}

void ReportPageObject::parseNode()
{
    //    _ *dictionary;
    //_ pageNodeSource->attributes().size();
    //_ pageNodeSource->attributes().namedItem("Name").nodeValue();

    QDomNode currPageNodesData, currSourceBand;
    for (int i = 0; i < pageNodesData->count() - 1; ++i)
    {
        //Перебор страниц данных
        currPageNodesData = pageNodesData->at(i);

        currSourceBand = pageNodeSource->firstChild();
        while(!currSourceBand.isNull())
        {
            //Перебираем все бенды из сурса(To Do, нужно оптимизировать)
            //И ищем для текущего бенда все данные из текущей страницы данных
            QString bandLocation = pageNodeSource->toElement().attribute("Name") + ".";
            bandLocation += currSourceBand.toElement().attribute("Name");
            _ bandLocation << " = " <<
                              dictionary->value(bandLocation);
            QDomNodeList dataBand = currPageNodesData.toElement().elementsByTagName(dictionary->value(bandLocation));
            _ "t = " << dataBand.item(0).toElement().attribute("t").toFloat();
            currSourceBand = currSourceBand.nextSibling();
        }

    }
}

float ReportPageObject::getTopOffset()
{
    return topOffset;
}

float ReportPageObject::getLeftOffset()
{
    return leftOffset;
}
